﻿namespace zadatak2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDisplay = new System.Windows.Forms.Label();
            this.upisano_slovo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.pokusaji_label = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtDisplay
            // 
            this.txtDisplay.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtDisplay.Location = new System.Drawing.Point(0, 0);
            this.txtDisplay.Name = "txtDisplay";
            this.txtDisplay.Size = new System.Drawing.Size(800, 67);
            this.txtDisplay.TabIndex = 0;
            this.txtDisplay.Text = "label1";
            this.txtDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // upisano_slovo
            // 
            this.upisano_slovo.Location = new System.Drawing.Point(43, 172);
            this.upisano_slovo.Name = "upisano_slovo";
            this.upisano_slovo.Size = new System.Drawing.Size(100, 20);
            this.upisano_slovo.TabIndex = 1;
            this.upisano_slovo.Enter += new System.EventHandler(this.provjera_slova);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Upišite 1 slovo";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(222, 168);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Provjeri";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.provjera_slova);
            // 
            // pokusaji_label
            // 
            this.pokusaji_label.AutoSize = true;
            this.pokusaji_label.Location = new System.Drawing.Point(43, 229);
            this.pokusaji_label.Name = "pokusaji_label";
            this.pokusaji_label.Size = new System.Drawing.Size(104, 13);
            this.pokusaji_label.TabIndex = 4;
            this.pokusaji_label.Text = "Imate još 6 pokušaja";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(222, 219);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Novi pojam";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.izaberNoviPojam);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pokusaji_label);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.upisano_slovo);
            this.Controls.Add(this.txtDisplay);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtDisplay;
        private System.Windows.Forms.TextBox upisano_slovo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label pokusaji_label;
        private System.Windows.Forms.Button button2;
    }
}

