﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zadatak2
{
    public partial class Form1 : Form
    {
        string[] pojmovi=new string[6];
        string odabrani_pojam;
        string zamaskirani_pojam;
        int broj_pokusaja = 6;
        string path = "D:\\analize\\analiza 6\\zadatak2\\pojmovi.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                int i = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    pojmovi[i] = line;
                    i++;
                }
            }
            noviPojam();
    
        }
        private void noviPojam()
        {
            Random rnd = new Random();
            int index = rnd.Next(0, 4);
            odabrani_pojam = pojmovi[index];
            zamaskirani_pojam = "";
            for (int i = 0; i < odabrani_pojam.Length;i++)
            {
                zamaskirani_pojam += "_";
            }
            txtDisplay.Text = zamaskirani_pojam;
        }

         private void provjera_slova(object sender, EventArgs e)
        {
             
             if(upisano_slovo.TextLength>1)
             {
                 MessageBox.Show("Upišite samo 1 slovo");
            }
             else if(upisano_slovo.TextLength==1)
             {
                 if(odabrani_pojam.Contains(upisano_slovo.Text))
                 {
                     char[] zamaskirani = zamaskirani_pojam.ToCharArray();
                     char[] odabrani = odabrani_pojam.ToCharArray();
                    char[] upisano = upisano_slovo.Text.ToCharArray();
                    int duljina = odabrani.Length;
                    
                   for(int i=0;i<=duljina-1;i++)
                    {
                        if(odabrani[i]==upisano[0])
                        {
                            zamaskirani[i] = odabrani[i];
                        }
                    }
                    string rezultat = new string(zamaskirani);
                    zamaskirani_pojam = rezultat;
                    txtDisplay.Text = zamaskirani_pojam;
                    if(zamaskirani_pojam.Contains(odabrani_pojam))
                    {
                        pokusaji_label.Text = "Pobjedili ste";
                    }
                }
                 else
                 {
                     broj_pokusaja--;
                     if(broj_pokusaja<=0)
                     {
                         pokusaji_label.Text = "Izgubili ste";
                        txtDisplay.Text = "Pojam je bio "+odabrani_pojam;
                     }
                     else
                     {
                         pokusaji_label.Text = "Imajte još " + broj_pokusaja.ToString() + " pokušaja";
                     }
                 }

            } 
        }

    private void izaberNoviPojam(object sender, EventArgs e)
        {
            noviPojam();
        }
    }
}
