﻿namespace zadatak1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.num_7 = new System.Windows.Forms.Button();
            this.num_8 = new System.Windows.Forms.Button();
            this.num_9 = new System.Windows.Forms.Button();
            this.DEL_button = new System.Windows.Forms.Button();
            this.AC_button = new System.Windows.Forms.Button();
            this.division_button = new System.Windows.Forms.Button();
            this.multiplication_button = new System.Windows.Forms.Button();
            this.num_6 = new System.Windows.Forms.Button();
            this.num_5 = new System.Windows.Forms.Button();
            this.num_4 = new System.Windows.Forms.Button();
            this.minus_button = new System.Windows.Forms.Button();
            this.plus_button = new System.Windows.Forms.Button();
            this.num_3 = new System.Windows.Forms.Button();
            this.num_2 = new System.Windows.Forms.Button();
            this.num_1 = new System.Windows.Forms.Button();
            this.result_button = new System.Windows.Forms.Button();
            this.change_sign = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.decimal_dot = new System.Windows.Forms.Button();
            this.num_0 = new System.Windows.Forms.Button();
            this.Display_1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // num_7
            // 
            this.num_7.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_7.Location = new System.Drawing.Point(13, 134);
            this.num_7.Name = "num_7";
            this.num_7.Size = new System.Drawing.Size(45, 34);
            this.num_7.TabIndex = 0;
            this.num_7.Text = "7";
            this.num_7.UseVisualStyleBackColor = false;
            this.num_7.Click += new System.EventHandler(this.valueset);
            // 
            // num_8
            // 
            this.num_8.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_8.Location = new System.Drawing.Point(64, 134);
            this.num_8.Name = "num_8";
            this.num_8.Size = new System.Drawing.Size(45, 34);
            this.num_8.TabIndex = 1;
            this.num_8.Text = "8";
            this.num_8.UseVisualStyleBackColor = false;
            this.num_8.Click += new System.EventHandler(this.valueset);
            // 
            // num_9
            // 
            this.num_9.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_9.Location = new System.Drawing.Point(115, 134);
            this.num_9.Name = "num_9";
            this.num_9.Size = new System.Drawing.Size(45, 34);
            this.num_9.TabIndex = 2;
            this.num_9.Text = "9";
            this.num_9.UseVisualStyleBackColor = false;
            this.num_9.Click += new System.EventHandler(this.valueset);
            // 
            // DEL_button
            // 
            this.DEL_button.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.DEL_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DEL_button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.DEL_button.Location = new System.Drawing.Point(166, 134);
            this.DEL_button.Name = "DEL_button";
            this.DEL_button.Size = new System.Drawing.Size(45, 34);
            this.DEL_button.TabIndex = 3;
            this.DEL_button.Text = "DEL";
            this.DEL_button.UseVisualStyleBackColor = false;
            this.DEL_button.Click += new System.EventHandler(this.DEL_button_Click);
            // 
            // AC_button
            // 
            this.AC_button.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.AC_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AC_button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AC_button.Location = new System.Drawing.Point(217, 134);
            this.AC_button.Name = "AC_button";
            this.AC_button.Size = new System.Drawing.Size(45, 34);
            this.AC_button.TabIndex = 4;
            this.AC_button.Text = "AC";
            this.AC_button.UseVisualStyleBackColor = false;
            this.AC_button.Click += new System.EventHandler(this.AC_button_Click);
            // 
            // division_button
            // 
            this.division_button.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.division_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.division_button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.division_button.Location = new System.Drawing.Point(217, 174);
            this.division_button.Name = "division_button";
            this.division_button.Size = new System.Drawing.Size(45, 34);
            this.division_button.TabIndex = 9;
            this.division_button.Text = "/";
            this.division_button.UseVisualStyleBackColor = false;
            this.division_button.Click += new System.EventHandler(this.operator_click);
            // 
            // multiplication_button
            // 
            this.multiplication_button.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.multiplication_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.multiplication_button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.multiplication_button.Location = new System.Drawing.Point(166, 174);
            this.multiplication_button.Name = "multiplication_button";
            this.multiplication_button.Size = new System.Drawing.Size(45, 34);
            this.multiplication_button.TabIndex = 8;
            this.multiplication_button.Text = "X";
            this.multiplication_button.UseVisualStyleBackColor = false;
            this.multiplication_button.Click += new System.EventHandler(this.operator_click);
            // 
            // num_6
            // 
            this.num_6.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_6.Location = new System.Drawing.Point(115, 174);
            this.num_6.Name = "num_6";
            this.num_6.Size = new System.Drawing.Size(45, 34);
            this.num_6.TabIndex = 7;
            this.num_6.Text = "6";
            this.num_6.UseVisualStyleBackColor = false;
            this.num_6.Click += new System.EventHandler(this.valueset);
            // 
            // num_5
            // 
            this.num_5.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_5.Location = new System.Drawing.Point(64, 174);
            this.num_5.Name = "num_5";
            this.num_5.Size = new System.Drawing.Size(45, 34);
            this.num_5.TabIndex = 6;
            this.num_5.Text = "5";
            this.num_5.UseVisualStyleBackColor = false;
            this.num_5.Click += new System.EventHandler(this.valueset);
            // 
            // num_4
            // 
            this.num_4.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_4.Location = new System.Drawing.Point(13, 174);
            this.num_4.Name = "num_4";
            this.num_4.Size = new System.Drawing.Size(45, 34);
            this.num_4.TabIndex = 5;
            this.num_4.Text = "4";
            this.num_4.UseVisualStyleBackColor = false;
            this.num_4.Click += new System.EventHandler(this.valueset);
            // 
            // minus_button
            // 
            this.minus_button.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.minus_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.minus_button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.minus_button.Location = new System.Drawing.Point(217, 214);
            this.minus_button.Name = "minus_button";
            this.minus_button.Size = new System.Drawing.Size(45, 34);
            this.minus_button.TabIndex = 14;
            this.minus_button.Text = "-";
            this.minus_button.UseVisualStyleBackColor = false;
            this.minus_button.Click += new System.EventHandler(this.operator_click);
            // 
            // plus_button
            // 
            this.plus_button.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.plus_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.plus_button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.plus_button.Location = new System.Drawing.Point(166, 214);
            this.plus_button.Name = "plus_button";
            this.plus_button.Size = new System.Drawing.Size(45, 34);
            this.plus_button.TabIndex = 13;
            this.plus_button.Text = "+";
            this.plus_button.UseVisualStyleBackColor = false;
            this.plus_button.Click += new System.EventHandler(this.operator_click);
            // 
            // num_3
            // 
            this.num_3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_3.Location = new System.Drawing.Point(115, 214);
            this.num_3.Name = "num_3";
            this.num_3.Size = new System.Drawing.Size(45, 34);
            this.num_3.TabIndex = 12;
            this.num_3.Text = "3";
            this.num_3.UseVisualStyleBackColor = false;
            this.num_3.Click += new System.EventHandler(this.valueset);
            // 
            // num_2
            // 
            this.num_2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_2.Location = new System.Drawing.Point(64, 214);
            this.num_2.Name = "num_2";
            this.num_2.Size = new System.Drawing.Size(45, 34);
            this.num_2.TabIndex = 11;
            this.num_2.Text = "2";
            this.num_2.UseVisualStyleBackColor = false;
            this.num_2.Click += new System.EventHandler(this.valueset);
            // 
            // num_1
            // 
            this.num_1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_1.Location = new System.Drawing.Point(13, 214);
            this.num_1.Name = "num_1";
            this.num_1.Size = new System.Drawing.Size(45, 34);
            this.num_1.TabIndex = 10;
            this.num_1.Text = "1";
            this.num_1.UseVisualStyleBackColor = false;
            this.num_1.Click += new System.EventHandler(this.valueset);
            // 
            // result_button
            // 
            this.result_button.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.result_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.result_button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.result_button.Location = new System.Drawing.Point(166, 254);
            this.result_button.Name = "result_button";
            this.result_button.Size = new System.Drawing.Size(96, 34);
            this.result_button.TabIndex = 19;
            this.result_button.Text = "=";
            this.result_button.UseVisualStyleBackColor = false;
            this.result_button.Click += new System.EventHandler(this.result_button_Click);
            // 
            // change_sign
            // 
            this.change_sign.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.change_sign.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.change_sign.ForeColor = System.Drawing.SystemColors.ControlText;
            this.change_sign.Location = new System.Drawing.Point(115, 254);
            this.change_sign.Name = "change_sign";
            this.change_sign.Size = new System.Drawing.Size(45, 34);
            this.change_sign.TabIndex = 18;
            this.change_sign.Text = "+/-";
            this.change_sign.UseVisualStyleBackColor = false;
            this.change_sign.Click += new System.EventHandler(this.change_sign_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button18.Location = new System.Drawing.Point(13, 94);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(45, 34);
            this.button18.TabIndex = 17;
            this.button18.Text = "sin";
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // decimal_dot
            // 
            this.decimal_dot.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.decimal_dot.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.decimal_dot.ForeColor = System.Drawing.SystemColors.ControlText;
            this.decimal_dot.Location = new System.Drawing.Point(64, 254);
            this.decimal_dot.Name = "decimal_dot";
            this.decimal_dot.Size = new System.Drawing.Size(45, 34);
            this.decimal_dot.TabIndex = 16;
            this.decimal_dot.Text = ".";
            this.decimal_dot.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.decimal_dot.UseVisualStyleBackColor = false;
            this.decimal_dot.Click += new System.EventHandler(this.valueset);
            // 
            // num_0
            // 
            this.num_0.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.num_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this.num_0.Location = new System.Drawing.Point(13, 254);
            this.num_0.Name = "num_0";
            this.num_0.Size = new System.Drawing.Size(45, 34);
            this.num_0.TabIndex = 15;
            this.num_0.Text = "0";
            this.num_0.UseVisualStyleBackColor = false;
            this.num_0.Click += new System.EventHandler(this.valueset);
            // 
            // Display_1
            // 
            this.Display_1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Display_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Display_1.Location = new System.Drawing.Point(0, 0);
            this.Display_1.Name = "Display_1";
            this.Display_1.Size = new System.Drawing.Size(284, 63);
            this.Display_1.TabIndex = 20;
            this.Display_1.Text = "0";
            this.Display_1.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(64, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(45, 34);
            this.button1.TabIndex = 21;
            this.button1.Text = "cos";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button2.Location = new System.Drawing.Point(115, 94);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(45, 34);
            this.button2.TabIndex = 22;
            this.button2.Text = "²";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button3.Location = new System.Drawing.Point(166, 94);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(45, 34);
            this.button3.TabIndex = 23;
            this.button3.Text = "√";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button4.Location = new System.Drawing.Point(217, 94);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(45, 34);
            this.button4.TabIndex = 24;
            this.button4.Text = "abs";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 294);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Display_1);
            this.Controls.Add(this.result_button);
            this.Controls.Add(this.change_sign);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.decimal_dot);
            this.Controls.Add(this.num_0);
            this.Controls.Add(this.minus_button);
            this.Controls.Add(this.plus_button);
            this.Controls.Add(this.num_3);
            this.Controls.Add(this.num_2);
            this.Controls.Add(this.num_1);
            this.Controls.Add(this.division_button);
            this.Controls.Add(this.multiplication_button);
            this.Controls.Add(this.num_6);
            this.Controls.Add(this.num_5);
            this.Controls.Add(this.num_4);
            this.Controls.Add(this.AC_button);
            this.Controls.Add(this.DEL_button);
            this.Controls.Add(this.num_9);
            this.Controls.Add(this.num_8);
            this.Controls.Add(this.num_7);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button num_7;
        private System.Windows.Forms.Button num_8;
        private System.Windows.Forms.Button num_9;
        private System.Windows.Forms.Button DEL_button;
        private System.Windows.Forms.Button AC_button;
        private System.Windows.Forms.Button division_button;
        private System.Windows.Forms.Button multiplication_button;
        private System.Windows.Forms.Button num_6;
        private System.Windows.Forms.Button num_5;
        private System.Windows.Forms.Button num_4;
        private System.Windows.Forms.Button minus_button;
        private System.Windows.Forms.Button plus_button;
        private System.Windows.Forms.Button num_3;
        private System.Windows.Forms.Button num_2;
        private System.Windows.Forms.Button num_1;
        private System.Windows.Forms.Button result_button;
        private System.Windows.Forms.Button change_sign;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button decimal_dot;
        private System.Windows.Forms.Button num_0;
        private System.Windows.Forms.Label Display_1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}

