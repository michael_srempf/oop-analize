﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zadatak1
{
    public partial class Form1 : Form
    {
        String operation = "";
        Double first_num, second_num;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Display_1.Text == "0")
            { Display_1.Text = ""; }
        }

        private void AC_button_Click(object sender, EventArgs e)
        {
            Display_1.Text = "";
        }

        private void DEL_button_Click(object sender, EventArgs e)
        {
            Display_1.Text = Display_1.Text.Remove(Display_1.Text.Length - 1);
        }

        private void operator_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            first_num = Double.Parse(Display_1.Text);
            operation = b.Text;
            Display_1.Text = "";
        }

        private void result_button_Click(object sender, EventArgs e)
        {
            second_num = Double.Parse(Display_1.Text);
            switch(operation)
            {
                case "+":
                    Display_1.Text = Convert.ToString(first_num + second_num);
                    break;
                case "-":
                    Display_1.Text = Convert.ToString(first_num - second_num);
                    break;
                case "X":
                    Display_1.Text = Convert.ToString(first_num * second_num);
                    break;
                case "/":
                    Display_1.Text = Convert.ToString(first_num / second_num);
                    break;
            }
        }

        private void change_sign_Click(object sender, EventArgs e)
        {

            if (Display_1.Text.Contains("-"))
            {
                Display_1.Text = Display_1.Text.Substring(1, Display_1.Text.Length - 1);
            }
            else
            {
                Display_1.Text = "-" + Display_1.Text;
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            first_num = double.Parse(Display_1.Text);
            Display_1.Text = Convert.ToString(Math.Abs(first_num));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            first_num = double.Parse(Display_1.Text);
            Display_1.Text = Convert.ToString(Math.Sqrt(first_num));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            first_num = double.Parse(Display_1.Text);
            Display_1.Text = Convert.ToString(Math.Pow(first_num,2));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            first_num = double.Parse(Display_1.Text);
            Display_1.Text = Convert.ToString(Math.Cos(first_num));
        }

        private void button18_Click(object sender, EventArgs e)
        {
            first_num = double.Parse(Display_1.Text);
            Display_1.Text = Convert.ToString(Math.Sin(first_num));
        }

        private void valueset(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if(b.Text==".")
            {
                if (!Display_1.Text.Contains(".") && Display_1.Text == "")
                {
                    Display_1.Text = "0" + b.Text;
                   
                }
                else if(!Display_1.Text.Contains("."))
                {
                    Display_1.Text = Display_1.Text + b.Text;
                }
            }
            else
            {
                Display_1.Text += b.Text;
            }
        }

    }
}
