﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ForSchoolWPF
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        public bool won = false;
        public string turn = "O";

        public GameWindow()
        {
            InitializeComponent();
            List<List<Button>> igra = new List<List<Button>>();
            List<Button> A = new List<Button>();
            A.Add(_1A);
            A.Add(_2A);
            A.Add(_3A);
            List<Button> B = new List<Button>();
            B.Add(_1B);
            B.Add(_2B);
            B.Add(_3B);
            List<Button> C = new List<Button>();
            C.Add(_1C);
            C.Add(_2C);
            C.Add(_3C);
            igra.Add(A);
            igra.Add(B);
            igra.Add(C);
            gameFields = igra;
            SwitchTurn();
        }

        void SwitchTurn()
        {
            if (turn == "O")
            {
                turn = "X";
                X.Background = Brushes.Blue;
                O.Background = Brushes.White;
            }
            else
            {
                turn = "O";
                O.Background = Brushes.Blue;
                X.Background = Brushes.White;
            }
        }

        private void _1A_Click(object sender, RoutedEventArgs e)
        {
            FillButton(_1A);
        }

        void FillButton(Button obj)
        {
            if (won)
            {
                won = false;
                ResetGame();
                return;
            }
            if (obj.Content == "")
            {
                obj.Content = turn;
                if (WinCond())
                {
                    if (turn == "X")
                    {
                        int num = Convert.ToInt32(Xc.Text);
                        num++;
                        Xc.Text = num.ToString();
                        X.Background = Brushes.Green;
                    }
                    else
                    {
                        int num = Convert.ToInt32(Oc.Text);
                        num++;
                        Oc.Text = num.ToString();
                        O.Background = Brushes.Green;
                    }
                    won = true;
                }
                else
                {
                    if (Draw())
                    {
                        won = true;
                        X.Background = Brushes.Gray;
                        O.Background = Brushes.Gray;
                        return;
                    }
                    SwitchTurn();
                }
            }
        }

        private void _2A_Click(object sender, RoutedEventArgs e)
        {
            FillButton(_2A);
        }

        private void _3A_Click(object sender, RoutedEventArgs e)
        {
            FillButton(_3A);
        }

        private void _1B_Click(object sender, RoutedEventArgs e)
        {
            FillButton(_1B);
        }

        private void _2B_Click(object sender, RoutedEventArgs e)
        {
            FillButton(_2B);
        }

        private void _3B_Click(object sender, RoutedEventArgs e)
        {
            FillButton(_3B);
        }

        private void _1C_Click(object sender, RoutedEventArgs e)
        {
            FillButton(_1C);
        }

        private void _2C_Click(object sender, RoutedEventArgs e)
        {
            FillButton(_2C);
        }

        private void _3C_Click(object sender, RoutedEventArgs e)
        {
            FillButton(_3C);
        }

        public List<List<Button>> gameFields = new List<List<Button>>();

        public bool WinCond()
        {
            List<List<Button>> igra = gameFields;
            for (var x = 0; x < 3; x++)
            {
                if (AllFieldsTheSame(x, 0, igra, 0, 1))
                    return true;
            }

            for (var y = 0; y < 3; y++)
                if (AllFieldsTheSame(0, y, igra, 1, 0))
                    return true;

            if (AllFieldsTheSame(0, 0, igra, 1, 1))
                return true;

            if (AllFieldsTheSame(2, 0, igra, -1, 1))
                return true;

            return false;
        }

        public static bool AllFieldsTheSame(int startX, int startY, List<List<Button>> igra, int dx, int dy)
        {
            string firstField = igra[startX][startY].Content.ToString();
            if (firstField == "")
            {
                return false;
            }

            for (var i = 0; i < 3; i++)
            {
                int y = startY + dy * i;
                int x = startX + dx * i;
                if (igra[x][y].Content.ToString() != firstField)
                {
                    return false;
                }
            }

            return true;
        }

        void ResetGame()
        {
            foreach (List<Button> lb in gameFields)
            {
                foreach (Button b in lb)
                {
                    b.Content = "";
                }
            }
            SwitchTurn();
        }

        bool Draw()
        {
            bool flaggie = true;
            foreach (List<Button> lb in gameFields)
            {
                foreach (Button b in lb)
                {
                    if (b.Content.ToString() == "")
                    {
                        flaggie = false;
                    }
                }
            }
            return flaggie;
        }
    }
}
